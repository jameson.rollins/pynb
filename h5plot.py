#!/usr/bin/env python

import sys
import h5py
import numpy as np
from matplotlib import pyplot as plt

plt.rc('text', usetex=True)
plt.rc('font', size=28, family='serif')
plt.rc('lines', linewidth=3)

figsize = (16,10)
axissize = [0.12, 0.11, 0.76, 0.83]

xlim = [7, 3000]
ylim = [1e-21, 1e-16]

colors = dict([
    ('measured', 'black'),
    ('quantum', 'blueviolet'),
    ('dark', 'brown'),
    ('seismic', 'green'),
    ('thermal', 'orangered'),
    ('actuator', 'hotpink'),
    ('electrostatic', 'lawngreen'),
    ('gas', 'thistle'),
    ('LSC', 'olive'),
    ('ASC', 'darkturquoise'),
    ('intensity+frequency', 'lightsalmon'),
    ('jitter', 'lightgreen'),
    ('sum', 'blue'),
    ])

##################################################

def h5_print(f):
    for k,v in f.attrs.items():
        print('{} = {}'.format(k,v))
    def pv(name):
        o = f[name]
        if type(o) is h5py.Dataset:
            print('{}: {}'.format(name, o.shape))
        else:
            print('{}/'.format(name))
    f.visit(pv)

def validate_nb_file(f):
    # FIXME: how to validate?
    if f.attrs['schema'] != 'aLIGO Noise Budget':
        raise ValueError("unexpected schema description")
    if type(f.attrs['schema_version']) is int:
        raise TypeError("version attribute incorrect type")
    h5_print(f)

def plot_trace(freq, data, style, label):
    plt.loglog(freq, data, style,
               label=label,
               color=colors[label])
        
def plot_h5(path, outfile=None):
    """plot ASD of noise budget HDF5 file

    """
    figs = []
    with h5py.File(path, 'r') as nb:
        validate_nb_file(nb)

        fig = plt.figure(figsize=figsize)
        ax = fig.add_axes(axissize)

        freq = nb['frequency']
        plot_trace(freq, nb['measured'], '-', 'measured')
        plot_trace(freq, nb['sum'], '-', 'sum')
        for label,data in nb['traces'].iteritems():
            if label in ['frequency', 'measured', 'sum']:
                continue
            plot_trace(freq, data, '--', label)

        figs.append(fig)

        plt.title('{} noise budget, {}'.format('H1', nb.attrs['date']))

    ax.set_xlim(xlim)
    ax.set_ylim(ylim)
    ax.grid(True)
    ax.set_xlabel(r'Frequency $\left[\mathrm{Hz}\right]$')
    ax.set_ylabel(r'Displacement $\left[\mathrm{m}/\sqrt{\mathrm{Hz}}\right]$')

    legend = plt.legend(loc='upper right',
                        facecolor='white',
                        fontsize='small',
                        framealpha=0.9,
                        #edgecolor='black',
                        )
    #legend.get_frame().set_color('white', alpha=False)

    ax2 = ax.twinx()
    ax2.set_yscale('log')
    ax2.set_ylim([y/4000 for y in ylim])
    #ax2.set_ylim(ylim)
    #ax2.set_yticks([y*4000. for y in ax.get_yticks()])
    #ax2.set_yticklabels(ax2.get_yticks())
    ax2.set_ylabel(r'Strain $\left[1/\sqrt{\mathrm{Hz}}\right]$')

    if outfile:
        plt.savefig(outfile)
    else:
        plt.show()

    return figs

##################################################

if __name__ == '__main__':

    path = sys.argv[1]
    try:
        outfile = sys.argv[2]
    except IndexError:
        outfile = None

    figs = plot_h5(path, outfile)
