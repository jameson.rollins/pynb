import os
import logging
#import importlib
import runpy
import inspect

from . import NoiseBudget

##################################################

def local_path(path):
    #print(__file__)
    # spath = inspect.getsourcefile(path)
    # sdir = os.path.dirname(spath)
    # fp = os.path.join(sdir, path)
    # print(fp)
    return path

def load_budget_module(location):
    name = os.path.basename(location)

    logging.info("loading module: %s" % (name))

    # https://docs.python.org/3/library/imp.html#imp.load_module
    #module = importlib.machinery.SourceFileLoader(name, location).load_module()

    # 3.5
    # print(name,location)
    # spec = importlib.util.spec_from_file_location(name, location)
    # print(spec)
    # module = importlib.util.module_from_spec(spec)
    # print(module)

    module = runpy.run_path(location)

    for key, obj in inspect.getmembers(module):
        print(key, obj)

    # mod = module.load_module()
    # print(mod)
    #importlib.find_loader(modname, path=modpath)
    #budget = NoiseBudget.from_module(modpath)
    #module = importlib.import_module(modname, package=modname)
    return module
