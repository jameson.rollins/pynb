import logging
#import importlib
import numpy as np

from .trace import NBTrace 

class NoiseBudget(object):
    """A NoiseBudget object

    """
    def __init__(self, traces, freq, title="Noise Budget"):
        self.traces = {}
        for trace in traces:
            t = trace()
            logging.info("adding trace: %s" % (t.name))
            # if not isinstance(trace, NBTrace):
            #     raise TypeError
            self.traces[t.name] = t
        logging.debug("traces: %s" % self.traces)
        self.__freq = freq
        self.title = title

    # @classmethod
    # def from_module(cls, modname):
    #     logging.info("loading module: %s" % (modname))
    #     module = importlib.import_module(modname)
    #     return cls(module.traces)

    def load(self, data_root='.'):
        """Load all data for traces

        """
        for name, trace in self.traces.items():
            logging.debug('load: '+name)
            trace.load(data_root)

    ##########

    @property
    def freq(self):
        return self.__freq

    def interp(self, freq=None):
        """Interpolate all traces at budget frequencies

        """
        if freq:
            self.freq = freq
        for name, trace in self.traces.items():
            logging.debug('interp: '+name)
            trace.interp(self.freq)
        
    ##########

    def __getitem__(self, name):
        return self.traces[name]

    def __iter__(self):
        #return iter(self.traces)
        return iter(self.traces.values())

    ##########

    @property
    def sum(self):
        try:
            return self.__sum.data
        except AttributeError:
            self.__sum = NBTrace.zeros(self.freq)
            for trace in self:
                logging.debug("adding trace: %s" % trace.name)
                logging.debug(" freq: %s [%s...%s]" % (len(trace.freq),
                                                      trace.freq[0],
                                                      trace.freq[-1]))
                logging.debug(" data: %s [%s...%s]" % (len(trace.data),
                                                      trace.data[0],
                                                      trace.data[-1]))
                try:
                    self.__sum += trace
                    logging.debug(" sum : %s [%s...%s]" % (len(self.__sum.data),
                                                         self.__sum.data[0],
                                                         self.__sum.data[-1]))
                except ValueError as e:
                    raise ValueError("%s: %s" % (trace.name, e))
            return self.__sum
